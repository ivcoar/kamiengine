package com.kamikaze.Entity;


import java.util.Random;

import org.lwjgl.util.vector.Vector3f;

import com.kamikaze.Engine.GLSL;
import com.kamikaze.Engine.Time;
import com.kamikaze.Model.Model;

public class Entity extends GameObject
{
	
	public Model model;
	Random rnd = new Random();
	Vector3f randVec;
	float rotSpeed;

	public Entity()
	{
		
	}
	
	@Override
	public void init()
	{
		this.model.init();
		rotSpeed = rnd.nextInt(720) - 360;
	}

	@Override
	public void update()
	{
		
		this.ang.y += rotSpeed * Time.deltaTime / 1000;
		
	}
	
	@Override
	public void delete()
	{
		
		this.model.delete();
		
	}
	
	public void render(GLSL glsl, GLSL textured)
	{
		
		this.model.setPos(this.pos);
		this.model.setAng(this.ang);
		this.model.setScale(this.scale);
		this.model.render(glsl, textured);
		
	}

}
