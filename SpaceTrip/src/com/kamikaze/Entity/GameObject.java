package com.kamikaze.Entity;

import org.lwjgl.util.vector.Vector3f;

public abstract class GameObject 
{
	
	Vector3f pos = new Vector3f();
	Vector3f ang = new Vector3f();
	Vector3f scale = new Vector3f(1, 1, 1);
	
	public Vector3f forward = new Vector3f();
	
	public abstract void init();
	public void preUpdate()
	{
		
		forward.x = (float) (Math.cos(Math.toRadians(this.ang.x)) * Math.sin(Math.toRadians(this.ang.y)));
		forward.y = (float) Math.sin(Math.toRadians(this.ang.x));
		forward.z = (float) (Math.cos(Math.toRadians(this.ang.x)) * Math.cos(Math.toRadians(this.ang.y)));
		
		this.update();
	
	}
	protected abstract void update();
	public abstract void delete();
	
	public void setPos(Vector3f vec)
	{
		pos = vec;
	}
	public void setPos(float x, float y, float z)
	{
		pos = new Vector3f(x, y, z);
	}
	public Vector3f getPos()
	{
		return pos;
	}
	public void setAng(Vector3f vec)
	{
		ang = vec;
	}
	public void setAng(float x, float y, float z)
	{
		ang = new Vector3f(x, y, z);
	}
	public Vector3f getAng()
	{
		return ang;
	}
	public void setScale(Vector3f vec)
	{
		scale = vec;
	}
	public void setScale(float x, float y, float z)
	{
		scale = new Vector3f(x, y, z);
	}
	public Vector3f getScale()
	{
		return scale;
	}
	
}
