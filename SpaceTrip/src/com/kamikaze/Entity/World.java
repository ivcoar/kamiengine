package com.kamikaze.Entity;

import java.util.ArrayList;
import java.util.List;


public class World extends GameObject
{
	
	private List<GameObject> contents = new ArrayList<GameObject>(); //Contents of the world
	
	public World(GameObject... gos)
	{
		
		for (GameObject go : gos)
		{
			contents.add(go);
		}
		
	}
	
	public World()
	{
		
		
		
	}

	@Override
	public void init() 
	{
		
	}

	@Override
	public void update()
	{
		
	}
	
	@Override
	public void delete()
	{
		
	}
	
}
