package com.kamikaze.Entity;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

import com.kamikaze.Engine.Time;

public class Camera extends GameObject
{
	
	float speed;
	float WALK_SPEED = 10f;
	float RUN_SPEED = 20f;
	float sensitivity = 10f;
	
	
	public Camera()
	{
		this.pos = new Vector3f(0,0,0);
	}
	
	public Camera(Vector3f pos) 
	{
		this.pos = pos;
	}

	@Override
	public void init()
	{
		
	}

	public void update()
	{
		
		boolean keyForward = Keyboard.isKeyDown(Keyboard.KEY_W);
		boolean keyBackward = Keyboard.isKeyDown(Keyboard.KEY_S);
		boolean keyLeft = Keyboard.isKeyDown(Keyboard.KEY_A);
		boolean keyRight = Keyboard.isKeyDown(Keyboard.KEY_D);
		boolean keyRun = Keyboard.isKeyDown(Keyboard.KEY_LSHIFT);
		
		//FPS Controls
		if (keyRun)
		{
			this.speed = RUN_SPEED;
		}else {
			this.speed = WALK_SPEED;
		}
		
		float dist = this.speed * Time.deltaTime / 1000; 
		
		if (keyForward && !keyBackward)
		{
			
			this.pos.x += dist * this.forward.x;
			this.pos.y -= dist * this.forward.y;
			this.pos.z += dist * this.forward.z;
		}
		if (keyBackward && !keyForward)
		{
			
			this.pos.x -= dist * this.forward.x;
			this.pos.y += dist * this.forward.y;
			this.pos.z -= dist * this.forward.z;
		}
		if (keyLeft && !keyRight)
		{
			this.pos.x += dist * Math.sin(Math.toRadians(this.ang.y + 90));
			this.pos.z += dist * Math.cos(Math.toRadians(this.ang.y + 90));
		}
		if (keyRight && !keyLeft)
		{
			this.pos.x -= dist * Math.sin(Math.toRadians(this.ang.y + 90));
			this.pos.z -= dist * Math.cos(Math.toRadians(this.ang.y + 90));
		}
		
		this.ang.x += Mouse.getDY() / sensitivity;
		this.ang.y -= Mouse.getDX() / sensitivity;
		
		if (this.ang.x < -90) this.ang.x = -90;
		if (this.ang.x > 90) this.ang.x = 90;
		
	}
	
	@Override
	public void delete()
	{
		
	}

}
