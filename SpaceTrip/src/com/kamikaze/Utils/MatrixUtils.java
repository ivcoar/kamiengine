package com.kamikaze.Utils;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public class MatrixUtils 
{
	
	//public Matrix4f projectionMatrix;
	//public Matrix4f viewMatrix;

	public static Matrix4f createProjectionMatrix(float fov, float aspect, float near, float far)
	{
		
		Matrix4f projMat = new Matrix4f();
		
		//float yScale = MathUtils.coTangent(MathUtils.degreesToRadians(fov / 2f));
		float yScale = MathUtils.coTangent((float) Math.toRadians(fov / 2f));
		float xScale = yScale / aspect;
		float frustumLength = far - near;
		
		projMat.m00 = xScale;
		projMat.m11 = yScale;
		projMat.m22 = -((far + near) / frustumLength);
		projMat.m23 = -1;
		projMat.m32 = -((2 * near * far) / frustumLength);
		projMat.m33 = 0;
		
		return projMat;
		
	}
	
	public static boolean isVisible(Matrix4f MVPmatrix, Vector3f pos, float radius)
	{
		
		//Ax + By + Cz + D = 0
		int A = 0;
		int B = 1;
		int C = 2;
		int D = 3;
		
		float x = pos.x;
		float y = pos.y;
		float z = pos.z;
		
		float plane[] = new float[4];
		
		//===============LEFT PLANE===============//
		plane[A] = MVPmatrix.m03 + MVPmatrix.m00;
		plane[B] = MVPmatrix.m13 + MVPmatrix.m10;
		plane[C] = MVPmatrix.m23 + MVPmatrix.m20;
		plane[D] = MVPmatrix.m33 + MVPmatrix.m30;
		
		//Normalise the plane
		float length = (float) Math.sqrt(plane[A] * plane[A] + plane[B] * plane[B] + 
				plane[C] * plane[C]);
		plane[A] /= length;
		plane[B] /= length;
		plane[C] /= length;
		plane[D] /= length;
		
		//Chech against the LEFT plane
		float dist = plane[A] * x + plane[B] * y + plane[C] * z + plane[D];
		if (dist <= -radius) return false;
		
		//===============RIGHT PLANE===============//
		plane[A] = MVPmatrix.m03 - MVPmatrix.m00;
		plane[B] = MVPmatrix.m13 - MVPmatrix.m10;
		plane[C] = MVPmatrix.m23 - MVPmatrix.m20;
		plane[D] = MVPmatrix.m33 - MVPmatrix.m30;
		
		//Normalise the plane
		length = (float) Math.sqrt(plane[A] * plane[A] + plane[B] * plane[B] + 
				plane[C] * plane[C]);
		plane[A] /= length;
		plane[B] /= length;
		plane[C] /= length;
		plane[D] /= length;
		
		//Check against the RIGHT plane
		dist = plane[A] * x + plane[B] * y + plane[C] * z + plane[D];
		if (dist <= -radius) return false;
		
		//===============BOTTOM PLANE===============//
		plane[A] = MVPmatrix.m03 + MVPmatrix.m01;
		plane[B] = MVPmatrix.m13 + MVPmatrix.m11;
		plane[C] = MVPmatrix.m23 + MVPmatrix.m21;
		plane[D] = MVPmatrix.m33 + MVPmatrix.m31;
		
		//Normalise the plane
		length = (float) Math.sqrt(plane[A] * plane[A] + plane[B] * plane[B] + 
				plane[C] * plane[C]);
		plane[A] /= length;
		plane[B] /= length;
		plane[C] /= length;
		plane[D] /= length;

		//Check against the BOTTOM plane
		dist = plane[A] * x + plane[B] * y + plane[C] * z + plane[D];
		if (dist <= -radius) return false;
		
		//===============TOP PLANE===============//
		plane[A] = MVPmatrix.m03 - MVPmatrix.m01;
		plane[B] = MVPmatrix.m13 - MVPmatrix.m11;
		plane[C] = MVPmatrix.m23 - MVPmatrix.m21;
		plane[D] = MVPmatrix.m33 - MVPmatrix.m31;
		
		//Normalise the plane
		length = (float) Math.sqrt(plane[A] * plane[A] + plane[B] * plane[B] + 
				plane[C] * plane[C]);
		plane[A] /= length;
		plane[B] /= length;
		plane[C] /= length;
		plane[D] /= length;

		//Check against the BOTTOM plane
		dist = plane[A] * x + plane[B] * y + plane[C] * z + plane[D];
		if (dist <= -radius) return false;
		
		//===============NEAR PLANE===============//
		plane[A] = MVPmatrix.m03 + MVPmatrix.m02;
		plane[B] = MVPmatrix.m13 + MVPmatrix.m12;
		plane[C] = MVPmatrix.m23 + MVPmatrix.m22;
		plane[D] = MVPmatrix.m33 + MVPmatrix.m32;
		
		//Normalise the plane
		length = (float) Math.sqrt(plane[A] * plane[A] + plane[B] * plane[B] + 
				plane[C] * plane[C]);
		plane[A] /= length;
		plane[B] /= length;
		plane[C] /= length;
		plane[D] /= length;

		//Check against the BOTTOM plane
		dist = plane[A] * x + plane[B] * y + plane[C] * z + plane[D];
		if (dist <= -radius) return false;
		
		//===============NEAR PLANE===============//
		plane[A] = MVPmatrix.m03 - MVPmatrix.m02;
		plane[B] = MVPmatrix.m13 - MVPmatrix.m12;
		plane[C] = MVPmatrix.m23 - MVPmatrix.m22;
		plane[D] = MVPmatrix.m33 - MVPmatrix.m32;

		//Normalise the plane
		length = (float) Math.sqrt(plane[A] * plane[A] + plane[B] * plane[B] + 
				plane[C] * plane[C]);
		plane[A] /= length;
		plane[B] /= length;
		plane[C] /= length;
		plane[D] /= length;

		//Check against the BOTTOM plane
		dist = plane[A] * x + plane[B] * y + plane[C] * z + plane[D];
		if (dist <= -radius) return false;
		
		//If we got here, the given sphere is in view
		return true;
		
	}
	
}
