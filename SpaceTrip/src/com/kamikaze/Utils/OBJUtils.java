package com.kamikaze.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.kamikaze.Model.Model;
import com.kamikaze.Utils.Vector.Face;
import com.kamikaze.Utils.Vector.Vector2;
import com.kamikaze.Utils.Vector.Vector3;
import com.kamikaze.Utils.Vector.Vertex;

public class OBJUtils 
{

	static ArrayList<String> parseOBJ(String location) throws IOException
	{
		
		BufferedReader reader = new BufferedReader(new FileReader(new File(location)));
		ArrayList<String> lines = new ArrayList<>();
		
		while (reader.ready())
		{
			lines.add(reader.readLine());
		}
		
		reader.close();
		reader = null;
		
		return lines;
		
	}
	
	public static Model loadOBJ(String location) throws IOException
	{
		
		Debug.debug("OBJUtils", "Opening OBJ in location '" + location + "'");
		
		ArrayList<Vector3> allVertices = new ArrayList<>();
		ArrayList<Vector3> allNormals = new ArrayList<>();
		ArrayList<Vector2> allTexCoords = new ArrayList<>();
		
		ArrayList<Face> faces = new ArrayList<>();
		
		Model model = new Model();
		
		ArrayList<String> lines = parseOBJ(location);
		
		Debug.debug("OBJUtils", "Reading OBJ contents");
		
		for (String line : lines)
		{
			
			if (line.startsWith("v "))
			{
				allVertices.add(new Vector3(Float.valueOf(line.split(" ")[1]), 
						Float.valueOf(line.split(" ")[2]), Float.valueOf(line.split(" ")[3])));
			}
			else if (line.startsWith("vt "))
			{
				allTexCoords.add(new Vector2(Float.valueOf(line.split(" ")[1]), 
						1 - Float.valueOf(line.split(" ")[2])));
			}
			else if (line.startsWith("vn "))
			{
				allNormals.add(new Vector3(Float.valueOf(line.split(" ")[1]), 
						Float.valueOf(line.split(" ")[2]), Float.valueOf(line.split(" ")[3])));
			}
			else if (line.startsWith("f "))
			{
				Face f = new Face();
				String[] faceVertexArray = line.split(" ");
				
				for (int index = 1; index < faceVertexArray.length; index++)
                {
                    String[] valueArray = faceVertexArray[index].split("/");

                    if (allTexCoords.size() > 0)
                        f.addVertex(new Vertex(allVertices.get(Integer.valueOf(valueArray[0]) - 1), allNormals.get(Integer.valueOf(valueArray[2]) - 1), allTexCoords.get(Integer.valueOf(valueArray[1]) - 1)));
                    else
                        f.addVertex(new Vertex(allVertices.get(Integer.valueOf(valueArray[0]) - 1), allNormals.get(Integer.valueOf(valueArray[2]) - 1), new Vector2(0, 0)));
                }
                faces.add(f);
			}
			
		}
		
		Debug.debug("OBJUtils", "Number of Vertices: " + allVertices.size());
		Debug.debug("OBJUtils", "Number of Normals: " + allNormals.size());
		Debug.debug("OBJUtils", "Number of TexCoords: " + allTexCoords.size());
		
		lines = null;
		allVertices = null;
		allNormals = null;
		allTexCoords = null;
		
		ArrayList<Vector3> VBOVertices = new ArrayList<Vector3>();
        ArrayList<Vector2> VBOTextureCoords = new ArrayList<Vector2>();
        ArrayList<Vector3> VBONormals = new ArrayList<Vector3>();
        ArrayList<Integer> VBOIndices = new ArrayList<Integer>();
		
		Debug.debug("OBJUtils", "Reorganizing indices");
		int counter = 0;
		for (Face f : faces)
		{
			for (Vertex v : f.vertices)
			{
				VBOVertices.add(v.position);
				VBONormals.add(v.normal);
				VBOTextureCoords.add(v.textureCoord);
				VBOIndices.add(counter);
				counter++;
			}
		}
		
		faces = null;
		model.createBuffers(integerListToShortArray(VBOIndices), Vector3.listToArray(VBOVertices), null, 
				Vector2.listToArray(VBOTextureCoords), Vector3.listToArray(VBONormals));
		
		VBOVertices = null;
        VBONormals = null;
        VBOTextureCoords = null;
        VBOIndices = null;
		
		return model;
	}
	
	public static short[] integerListToShortArray(ArrayList<Integer> list)
    {
        short[] returnArray = new short[list.size()];
        int counter = 0;
        for (int i : list)
        {
            returnArray[counter] = (short)i;
            counter++;
        }
        return returnArray;
    }
	
}
