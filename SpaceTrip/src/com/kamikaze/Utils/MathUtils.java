package com.kamikaze.Utils;

public class MathUtils 
{

	public static float coTangent(float ang)
	{
		return 1.0f / (float) Math.tan(ang);
	}
	
	public static float degreesToRadians(float ang)
	{
		
		return ang * (float) Math.PI / 180.0f;
		
	}
	
}
