package com.kamikaze.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Debug
{
	
	public boolean DEBUG = true;
	public boolean LOG = true;
	public static File logFile = new File("log.txt");
	static FileWriter logWriter;
	static BufferedWriter logger;
	//public boolean ADVANCED_MODE = true;
	
	public static void start()
	{
		try 
		{
			
			if (!logFile.exists())
			{
				logFile.createNewFile();
			}
			
			logWriter = new FileWriter(logFile);
			logger = new BufferedWriter(logWriter);
			
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	static void log(String text)
	{
		try {
			logger.append(text);
			logger.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void stop()
	{
		try 
		{
			logger.close();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void debug(String caller, String text)
	{
		
		System.out.println("[" + caller + "] " + text);
		log("[" + caller + "] " + text);
		
	}
	
	public static void info(String caller, String text)
	{
		
		System.out.println("[INFO] - " + "[" + caller + "] " + text);
		log("[INFO] - " + caller + ": " + text);
		
	}
	
	public static void warning(String caller,String text)
	{
		
		System.out.println("[WARNING] - " + "[" + caller + "] " + text);
		log("[WARNING] - " + caller + ": " + text);
		
	}

}
