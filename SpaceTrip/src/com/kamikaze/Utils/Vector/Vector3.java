package com.kamikaze.Utils.Vector;

import java.util.ArrayList;

public class Vector3
{

	public float x, y, z;
	
	public Vector3()
	{
		this.set(0, 0, 0);
	}
	
	public Vector3(float x, float y, float z)
	{
		this.set(x, y, z);
	}
	
	public void set(float x, float y, float z)
	{
		
		this.x = x;
		this.y = y;
		this.z = z;
		
	}
	
	public static float[] listToArray(ArrayList<Vector3> list)
	{
		
		float[] array = new float[list.size() * 3];
		
		int counter = 0;
		for (Vector3 v : list)
		{
			array[counter] = v.x;
			counter++;
			array[counter] = v.y;
			counter++;
			array[counter] = v.z;
			counter++;
		}
		
		return array;
		
	}
	
}
