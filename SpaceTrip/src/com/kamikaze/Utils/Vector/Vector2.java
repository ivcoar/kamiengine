package com.kamikaze.Utils.Vector;

import java.util.ArrayList;

public class Vector2
{

	public float x, y;
	
	public Vector2()
	{
		this.set(0, 0);
	}
	
	public Vector2(float x, float y)
	{
		this.set(x, y);
	}
	
	public void set(float x, float y)
	{
		
		this.x = x;
		this.y = y;
		
	}
	
	public static float[] listToArray(ArrayList<Vector2> list)
	{
		
		float[] array = new float[list.size() * 2];
		
		int counter = 0;
		for (Vector2 v : list)
		{
			array[counter] = v.x;
			counter++;
			array[counter] = v.y;
			counter++;
		}
		
		return array;
		
	}
	
}
