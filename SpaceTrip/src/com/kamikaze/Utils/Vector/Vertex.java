package com.kamikaze.Utils.Vector;

public class Vertex 
{
	
	public Vector3 position, normal;
	public Vector2 textureCoord;

	public Vertex(Vector3 pos, Vector3 normal, Vector2 texCoord)
	{
		this.position = pos;
		this.normal = normal;
		this.textureCoord = texCoord;
	}
	
}
