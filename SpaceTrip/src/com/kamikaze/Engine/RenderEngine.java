package com.kamikaze.Engine;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;


import com.kamikaze.Entity.Camera;
import com.kamikaze.Entity.Entity;
import com.kamikaze.Entity.GameObject;
import com.kamikaze.Model.Model;
import com.kamikaze.Utils.MathUtils;
import com.kamikaze.Utils.MatrixUtils;
import com.kamikaze.Utils.OBJUtils;


public class RenderEngine 
{

	private Camera camera;
	
	//Render Data
	public List<GameObject> renderList;
	public int visibleObjects;
	
	Vector3f camPos = new Vector3f();
	GLSL glsl;
	GLSL textured;
	
	Entity renderCube;

	//Matrix
	public float fov;
	public float aspect;
	public float near;
	public float far;
	Matrix4f projectionMatrix;
	Matrix4f viewMatrix;
	Matrix4f mvpMatrix;
	FloatBuffer matrixBuffer;
	
	//FBO
	int FBO_id;
	int FBO_tex_id;
	int FBO_renderBuffer_id;
	FrameBuffer fbo;
	
	Random rnd;
	
	public RenderEngine()
	{
		
		renderList = new ArrayList<GameObject>();
		rnd = new Random();
	}
	
	public void init()
	{
		
		//===============FBO===============//
		
		//fbo = new FrameBuffer(512, 512);
		//fbo.setupQuad();
		
		//===================================//
		
		camera.init();
		camera.setPos(0, 0, -10);
		
		glsl = new GLSL("vertex.vs", "fragment.fs");
		glsl.init();
		textured = new GLSL("vertex.vs", "textured.fs");
		textured.init();
		
		String[] modelLoc = {"model/cubo.obj", "model/mono.obj", "model/ship.obj", "model/sphere.obj"};
		
		Model[] models = null;
		try {
			models = new Model[]{OBJUtils.loadOBJ(modelLoc[0]), OBJUtils.loadOBJ(modelLoc[1]), 
					OBJUtils.loadOBJ(modelLoc[2]), OBJUtils.loadOBJ(modelLoc[3])};
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for (Model m : models)
		{
			if (m == models[0])
			{
				m.loadPNGTex(new File("tex/tex2.png"));
			}
			else if (m == models[1])
			{
				m.loadPNGTex(new File("tex/hair.png"));
			}
			else
			{
				float[] colors = new float[m.numOfVertices];
				for (int j = 0; j < colors.length; j++)
				{
					colors[j] = rnd.nextFloat();
				}
				
				m.setColors(colors);
			}
		}
		
		for (int i = 0; i < 500; i++)
		{
			Entity ent = new Entity();
			ent.model = models[rnd.nextInt(models.length)];
			
			ent.setPos(new Vector3f((rnd.nextFloat() * 100) - 50, (rnd.nextFloat() * 100) - 50, 
					(rnd.nextFloat() * 100) - 50));
			ent.setAng(new Vector3f(rnd.nextFloat() * 360, rnd.nextFloat() * 360, rnd.nextFloat() * 360));
			
			ent.init();
			
			renderList.add(ent);
		}
		
		renderCube = new Entity();
		try {
			renderCube.model = OBJUtils.loadOBJ("model/cubo.obj");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		/*
		renderCube.init();
		renderCube.model.texID = fbo.texture;
		renderCube.model.texWidth = 512;
		renderCube.model.texHeight = 512;
		*/
		
		//=======================MATRICES===========================//
		matrixBuffer = BufferUtils.createFloatBuffer(16);
		
		//Projection matrix
		projectionMatrix = MatrixUtils.createProjectionMatrix(fov, aspect, near, far);
		
		//ViewMatrix
		viewMatrix = new Matrix4f();
		
		//MVP Matrix
		mvpMatrix = new Matrix4f();
		
	}

	public void render()
	{
		//===============FBO===============//
		//fbo.setActive();
		//=================================//
		glClearColor(0.4f, 0.4f, 1f, 1f);
		
		camera.preUpdate();

		viewMatrix.setIdentity();
		Matrix4f.scale(camera.getScale(), viewMatrix, viewMatrix);
		Matrix4f.rotate(MathUtils.degreesToRadians(-camera.getAng().x), new Vector3f(1, 0, 0),  viewMatrix,  viewMatrix);
		Matrix4f.rotate(MathUtils.degreesToRadians(-camera.getAng().y), new Vector3f(0, 1, 0),  viewMatrix,  viewMatrix);
		Matrix4f.rotate(MathUtils.degreesToRadians(-camera.getAng().z), new Vector3f(0, 0, 1),  viewMatrix,  viewMatrix);
		Matrix4f.translate(new Vector3f(camera.getPos().x, camera.getPos().y, camera.getPos().z), viewMatrix, viewMatrix);
		
		//Gets the MVP Matrix
		Matrix4f.mul(projectionMatrix, viewMatrix, mvpMatrix);
		
		glsl.setProjViewMatrix(projectionMatrix, viewMatrix);
		glsl.updatePVMatrix();
		
		textured.setProjViewMatrix(projectionMatrix, viewMatrix);
		textured.updatePVMatrix();
		
		//===============UPDATE & RENDER===============//
		visibleObjects = 0;
		
		for (GameObject go : renderList)
		{
			go.preUpdate();
			if (go instanceof Entity)
			{
				if (MatrixUtils.isVisible(mvpMatrix, go.getPos(), ((Entity) go).model.radius))
				{
					((Entity) go).render(glsl, textured);
					visibleObjects++;
				}
			}
		}
		
		//fbo.drawQuad();
	}
	
	public void finish()
	{
		
		System.out.println("Deleting GLSL program");
		
		for (GameObject go : renderList)
		{
			go.delete();
			
		}
		glDeleteProgram(glsl.program);
		glDeleteProgram(textured.program);
		
	}
	
	public void setCamera(Camera camera)
	{
		
		this.camera = camera;
		
	}
	
	public Camera getCamera()
	{
		
		return this.camera;
		
	}
	
}
