package com.kamikaze.Engine;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.opengl.PixelFormat;

import com.kamikaze.Utils.Debug;

import static org.lwjgl.opengl.GL11.*;

public class DisplayManager
{
	
	//Window settings
	public int width;
	public int height;
	public String title;
	
	//Render settings
	public float fov;
	public float near;
	public float far;
	public float aspect;
	public boolean fboEnabled;
	
	//Fixed FPS
	public int targetFPS = 60;
	
	private RenderEngine renderEngine;
	
	public DisplayManager(int width, int height, String title, RenderEngine re)
	{
		
		Debug.info("DisplayManager", "Setting up DisplayManager: " + width + ", " + height + ", " + title);
		
		//this.width = width;
		//this.height = height;
		this.title = title;
		this.renderEngine = re;
		
		this.fov = 60f;
		this.near = 0.1f;
		this.far = 1000f;
		this.aspect = 16.0f / 9.0f;
		
	}
	
	public void start()
	{
		
		Debug.info("DisplayManager", "Starting Display...");
		
		PixelFormat pixelFormat = new PixelFormat();
		ContextAttribs contextAttributes = new ContextAttribs(3, 3).withForwardCompatible(true)
				.withProfileCore(true);
		
		try
		{
			
			Object[] dm = this.askMode();
			if (dm == null) return;
			DisplayMode displayMode = (DisplayMode) dm[0];
			
			this.width = displayMode.getWidth();
			this.height = displayMode.getHeight();
			
			//Initializes the Display
			Display.setDisplayMode(displayMode);
			Display.setFullscreen((boolean) dm[1]);
			Display.setTitle(title);
			Display.create(pixelFormat, contextAttributes);
			
			//Initializes the Mouse
			Mouse.create();
			Mouse.setGrabbed(true);
			
		}catch(LWJGLException e)
		{
			
			throw new RuntimeException("Could not initialize LWJGL", e);
			
		}
		
		Debug.info("DisplayManager", "OpenGL version: " + glGetString(GL_VERSION));
		fboEnabled = GLContext.getCapabilities().GL_EXT_framebuffer_object;
		if (fboEnabled)
		{
			Debug.info("DisplayManager", "FBO is enabled");
		}else{
			Debug.warning("DisplayManager", "FBO is enabled");
		}
		
		this.initGL(); //Initializes the OpenGL environmet
		
		Time.init(); //Initializes the Time variables
		
		renderEngine.fov = this.fov;
		renderEngine.aspect = this.aspect;
		renderEngine.near = this.near;
		renderEngine.far = this.far;
		renderEngine.init(); //Initializes the Render variables
		
		while (!Display.isCloseRequested())
		{
			
			gameLoop();
			glCullFace(GL_BACK);
			
			if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
			{
				break;
			}
			
		}
		
		System.out.println("Stopping Display...");
		this.finish();
		Display.destroy();
		
	}
	
	public void gameLoop()
	{
		
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT); //Clears the buffers
		
		renderEngine.render(); //Calls the RenderEngine to render
		
		Time.update();
		
		Display.setTitle(title + " FPS: " + Time.getFPS() + " Visible objects: " + renderEngine.visibleObjects + "/" +
				renderEngine.renderList.size());
		
		Display.update();//Updates the Display
		
		//Stops the rendering for having 60 FPS
		try
		{
			long waitTime = Time.getLastFrame() - Time.getTime() + (1000 / targetFPS);
			if (waitTime > 0) Thread.sleep(waitTime);
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		
	}
	
	public void initGL()
	{
		
		glViewport(0, 0, width, height);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		
	}
	
	public void finish()
	{
		
		renderEngine.finish();
		
	}
	
	public Object[] askMode() throws LWJGLException
	{
		
		DisplayMode[] modes = Display.getAvailableDisplayModes();
		String[] modeStrings = new String[modes.length + 1];

		modeStrings[modes.length] = "Windowed";
		
		for (int i = 0; i < modes.length; i++) 
		{
		    DisplayMode current = modes[i];
		    modeStrings[i] = i + "-" + current.getWidth() + "x" + current.getHeight() + "x" +
                    current.getBitsPerPixel() + " " + current.getFrequency() + "Hz";
		    Debug.debug("DisplayManager", "FullScreen: " + modeStrings[i]);
		}
		
		JFrame frame = new JFrame();
		frame.setVisible(true);
		String selection = (String) JOptionPane.showInputDialog(frame, "Select a Display Mode...", "Display Mode", 
				JOptionPane.PLAIN_MESSAGE, null, modeStrings, modeStrings[modes.length]);
		
		if (selection != null && selection != modeStrings[modes.length])
		{
			int index = Integer.parseInt(selection.split("-")[0]);
			Debug.debug("DisplayManager", "FullScreen: " + modeStrings[index]);
			
			//finalDisplayMode = modes[index];
			
			frame.dispose();
			
			Object[] dm = {modes[index], true};
			return dm;
			
		}else if (selection == modeStrings[modes.length])
		{
			String resolution = (String) JOptionPane.showInputDialog(frame, "Type in the resolution (WxH)", "Resolution",
					JOptionPane.PLAIN_MESSAGE, null, null, "1280x720");
			int width = Integer.parseInt(resolution.split("x")[0]);
			int height = Integer.parseInt(resolution.split("x")[1]);
			
			Debug.debug("DisplayManager", "Windowed Mode: " + width + " x " + height);
			
			//finalDisplayMode = new DisplayMode(width, height);
			
			frame.dispose();
			
			Object[] dm = {new DisplayMode(width, height), false};
			return dm;
			
		}
		
		frame.dispose();
		
		return null;
		
	}
	
}
