package com.kamikaze.Engine;

import org.lwjgl.Sys;

public class Time 
{
	
	public static long lastFrame;
	private static long lastFPS;
	private static int currentFPS;
	private static int finalFPS;
	
	public static long deltaTime;
	
	public static void init()
	{
		
		lastFPS = getTime();
		lastFrame = getTime();
	}
		
	public static void update()
	{
		
		updateFPS();
		deltaTime = deltaTime();
		
	}
	
	/**
	 * Gets the time in milliseconds
	 * 
	 * @return The System time in milliseconds
	 */
	public static long getTime()
	{
		
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
		
	}
	
	/**
	 * Gets the delta time (time between frames) in milliseconds
	 * 
	 * @return
	 */
	private static long deltaTime()
	{
		
		long time = getTime();
		
		long delta = (time - lastFrame);
		lastFrame = time;
		
		return delta;
		
	}
	
	
	
	
	
	private static void updateFPS() 
	{
	    if (getTime() - lastFPS > 1000) 
	    {
	    	finalFPS = currentFPS;
	    	currentFPS = 0;
	    	lastFPS += 1000;
	    }
	    currentFPS++;
	}
	
	public static int getFPS()
	{
		
		return finalFPS;
	
	}
	
	public static long getLastFrame()
	{
		
		return lastFrame;
		
	}
	
}
