package com.kamikaze.Engine;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;

public class GLSL 
{
	
	public int program;
	
	int uniform_mat_projection;
	int uniform_mat_view;
	int uniform_mat_model;
	
	Matrix4f matModel;
	Matrix4f matProjection;
	Matrix4f matView;
	FloatBuffer matBuffer;
	
	Map<String, Integer> attributes;
	
	String vertexName;
	String fragmentName;

	public GLSL(String vertex, String fragment)
	{
		
		this.vertexName = vertex;
		this.fragmentName = fragment;
		
		this.attributes = new HashMap<>();
		
		this.matModel = new Matrix4f();
		this.matProjection = new Matrix4f();
		this.matView = new Matrix4f();
		this.matBuffer = BufferUtils.createFloatBuffer(16);
		
	}
	
	public void init()
	{
		int link_ok = GL_FALSE;
		
		//GLSL Vertex Shader
		int vs = glCreateShader(GL_VERTEX_SHADER);
		String vs_source = this.loadShader("shader/" + vertexName);
		glShaderSource(vs, vs_source);
		glCompileShader(vs);
		//compile_ok = glGetShaderi(vs, GL_COMPILE_STATUS);
		if (glGetShaderi(vs, GL_COMPILE_STATUS) == GL_FALSE){
			System.out.println("Error in Vertex Shader");
			return;
		}
		else {
			System.out.println("Vertex Shader compiled succesfully");
		}
		
		//GLSL Fragment Shader
		int fs = glCreateShader(GL_FRAGMENT_SHADER);
		String fs_source = this.loadShader("shader/" + fragmentName);
		glShaderSource(fs, fs_source);
		glCompileShader(fs);
		if (glGetShaderi(vs, GL_COMPILE_STATUS) == GL_FALSE){
			System.out.println("Error in Fragment Shader");
			return;
		}
		else {
			System.out.println("Fragment Shader compiled succesfully");
		}
		
		//GLSL program
		program = glCreateProgram();
		glAttachShader(program, vs);
		glAttachShader(program, fs);
		
		//glBindFragDataLocation(program, 0, "outputF");
		
		glLinkProgram(program);
		link_ok = glGetProgrami(program, GL_LINK_STATUS);
		if (link_ok == 0){
			System.out.println("Could not link GLSL Program");
			return;
		}
		else {
			System.out.println("GLSL Program linked succesfully");
		}
		
		//Uniforms
		String uniform_name;
		uniform_name = "mat_projection";
		uniform_mat_projection = glGetUniformLocation(program, uniform_name);
		if (uniform_mat_projection == -1)
		{
			System.out.println("Could not bind Uniform");
			return;
		}
		
		uniform_name = "mat_view";
		uniform_mat_view = glGetUniformLocation(program, uniform_name);
		if (uniform_mat_view == -1)
		{
			System.out.println("Could not bind Uniform");
			return;
		}
		
		uniform_name = "mat_model";
		uniform_mat_model = glGetUniformLocation(program, uniform_name);
		if (uniform_mat_model == -1)
		{
			System.out.println("Could not bind Uniform");
			return;
		}
		
	}
	
	public void updatePVMatrix()
	{
		
		glUseProgram(this.program);
		matProjection.store(matBuffer); matBuffer.flip();
		glUniformMatrix4(this.uniform_mat_projection, false, matBuffer);
		matView.store(matBuffer); matBuffer.flip();
		glUniformMatrix4(this.uniform_mat_view, false, matBuffer);
		
	}
	
	public void updateModelMatrix()
	{
		
		glUseProgram(this.program);
		matModel.store(matBuffer); matBuffer.flip();
		glUniformMatrix4(this.uniform_mat_model, false, matBuffer);
		
	}
	
	public String loadShader(String shaderFile)
	{
		
		StringBuilder shader = new StringBuilder();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(shaderFile));
			
			String line;
			while ((line = reader.readLine()) != null)
			{
				shader.append(line).append("\n");
			}
			reader.close();
			return shader.toString();
		} catch (IOException e) 
		{
			System.err.println("Could not load Shader");
			e.printStackTrace();
			return null;
		}
		
	}
	
	public void setModelMatrix(Matrix4f mat)
	{
		this.matModel = mat;
	}
	
	public void setProjViewMatrix(Matrix4f matP, Matrix4f matV)
	{
		this.matProjection = matP;
		this.matView = matV;
	}
	
}
