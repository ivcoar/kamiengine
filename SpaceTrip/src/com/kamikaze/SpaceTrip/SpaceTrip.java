package com.kamikaze.SpaceTrip;

import org.lwjgl.util.vector.Vector3f;

import com.kamikaze.Engine.RenderEngine;
import com.kamikaze.Entity.Camera;

public class SpaceTrip 
{

	public RenderEngine renderEngine;
	public Camera camera;
	
	public SpaceTrip()
	{
		
		renderEngine = new RenderEngine();
		
		camera = new Camera(new Vector3f(0, 0, 10));
		
		renderEngine.setCamera(camera);
		
	}
	
}
