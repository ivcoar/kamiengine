 package com.kamikaze.Starter;

import java.io.File;

import com.kamikaze.Engine.DisplayManager;
import com.kamikaze.SpaceTrip.SpaceTrip;
import com.kamikaze.Utils.Debug;

public class Starter 
{
	
	static int width;
	static int height;
	
	public static String OS = System.getProperty("os.name").toLowerCase();  //Gets the OS name

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		
		Debug.start();
		
		Debug.info("Starter", "Starting...");
		
		//Loads native libraries
		System.setProperty("org.lwjgl.librarypath", new File("lwjgl/native/").getAbsolutePath());
		
		//Creates an instance of Space trip
		SpaceTrip spaceTrip = new SpaceTrip();
		
		//Creates the Display and shows it
		DisplayManager display = new DisplayManager(width, height, "SpaceTrip v0.1", spaceTrip.renderEngine);
		display.start();
		
		//System.out.println("Closing");
		Debug.info("Starter", "Closing...");
		
		Debug.stop();
		
		System.exit(0);

	}

}
