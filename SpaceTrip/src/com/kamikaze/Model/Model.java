package com.kamikaze.Model;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import com.kamikaze.Engine.GLSL;
import com.kamikaze.Utils.MathUtils;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

public class Model 
{
	
	//Render settings
	public float radius;
	
	//Matrices
	private Vector3f pos = new Vector3f(0, 0, 0);
	private Vector3f ang = new Vector3f(0, 0, 0);
	private Vector3f scale = new Vector3f(1, 1, 1);
	
	Matrix4f modelMatrix = new Matrix4f();
	FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
	
	//===============BUFFERS===============//
	//Indices
	public int numOfIndices;
	public ShortBuffer indices;
	
	//Vertices
	public int numOfVertices;
	public FloatBuffer vertices = null;
	
	//Colors
	public FloatBuffer colors = null;
	
	//Normals
	public FloatBuffer normals = null;
	
	//TexCoords
	public FloatBuffer texCoords = null;
	
	//Texture
	public int texID;
	public ByteBuffer texture2d = null;
	public int texWidth;
	public int texHeight;
	
	//VAO
	public int vaoID;
	
	//VBO
	public IntBuffer vboID;
	public short vboID_index;
	public short vboID_vertex;
	public short vboID_color;
	public short vboID_texCoord;
	public short vboID_normal;
	
	//Constants
	final int VERTEX_INDEX = 0;
	final int COLOR_INDEX = 1;
	final int TEX_INDEX = 2;
	final int NORMAL_INDEX = 3;
	
	public Model()
	{
		
	}
	
	public void init()
	{
		
		//VAO Setup
		vaoID = glGenVertexArrays(); //0 - Element; 1 - Vertex; 2 - Color
		glBindVertexArray(vaoID);
		
		//VBO Setup
		vboID = BufferUtils.createIntBuffer(5);
		glGenBuffers(vboID); //0 - Element; 1 - Vertex; 2 - Color; 3 - TexCoords; 4 - Normals;
		
		vboID_index = (short) vboID.get(0);
		vboID_vertex = (short) vboID.get(1);
		vboID_color = (short) vboID.get(2);		
		vboID_texCoord = (short) vboID.get(3);
		vboID_normal = (short) vboID.get(4);
		
		//Vectors
		glBindBuffer(GL_ARRAY_BUFFER, vboID_vertex);
		glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);
		glVertexAttribPointer(VERTEX_INDEX, 3, GL_FLOAT, false, 0, 0);
		
		//Colors
		if (this.colors != null)
		{
			glBindBuffer(GL_ARRAY_BUFFER, vboID_color);
			glBufferData(GL_ARRAY_BUFFER, colors, GL_STATIC_DRAW);
			glVertexAttribPointer(COLOR_INDEX, 3, GL_FLOAT, false, 0, 0);
		}
		//TexCoords
		if (this.texCoords != null)
		{
			glBindBuffer(GL_ARRAY_BUFFER, vboID_texCoord);
			glBufferData(GL_ARRAY_BUFFER, texCoords, GL_STATIC_DRAW);
			glVertexAttribPointer(TEX_INDEX, 2, GL_FLOAT, false, 0, 0);
		}
		//Normals
		if (this.normals != null)
		{
			glBindBuffer(GL_ARRAY_BUFFER, vboID_normal);
			glBufferData(GL_ARRAY_BUFFER, normals, GL_STATIC_DRAW);
			glVertexAttribPointer(NORMAL_INDEX, 3, GL_FLOAT, false, 0, 0);
		}

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		
		//Elements
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID_index);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, GL_STATIC_DRAW);
			
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		
		//Texture
		if (texture2d != null)
		{
			
			texID = glGenTextures();
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texID);
			
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texWidth, texHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture2d);
			glGenerateMipmap(GL_TEXTURE_2D);
			
			//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
			
			//glActiveTexture(0);
			//glBindTexture(GL_TEXTURE_2D, 0);
			
		}
		
	}
	
	public void render(GLSL def, GLSL textured)
	{
		
		//Model Matrix
		modelMatrix.setIdentity();
		Matrix4f.translate(getPos(), modelMatrix, modelMatrix);
		Matrix4f.scale(getScale(), modelMatrix, modelMatrix);
		Matrix4f.rotate(MathUtils.degreesToRadians(this.getAng().x), new Vector3f(1, 0, 0), modelMatrix, modelMatrix);
		Matrix4f.rotate(MathUtils.degreesToRadians(this.getAng().y), new Vector3f(0, 1, 0), modelMatrix, modelMatrix);
		Matrix4f.rotate(MathUtils.degreesToRadians(this.getAng().z), new Vector3f(0, 0, 1), modelMatrix, modelMatrix);
		
		GLSL shader = colors == null ? textured : def;
		//GLSL shader = def;
		
		//Rendering
		glUseProgram(shader.program);
		
		shader.setModelMatrix(modelMatrix);
		shader.updateModelMatrix();
		
		glBindAttribLocation(shader.program, VERTEX_INDEX, "pos");
		if (this.colors != null) glBindAttribLocation(shader.program, COLOR_INDEX, "color");
		if (this.normals != null) glBindAttribLocation(shader.program, NORMAL_INDEX, "normal");
		if (this.texCoords != null) glBindAttribLocation(shader.program, TEX_INDEX, "texCoord");
	
		glBindVertexArray(vaoID);
		
		//Vertices & Colors
		int loc = glGetUniformLocation(shader.program, "tex_diffuse");
		glUniform1i(loc, GL_TEXTURE0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texID);
		
		glEnableVertexAttribArray(VERTEX_INDEX);
		if (this.colors != null) glEnableVertexAttribArray(COLOR_INDEX);
		if (this.normals != null) glEnableVertexAttribArray(NORMAL_INDEX);
		if (this.texCoords != null) glEnableVertexAttribArray(TEX_INDEX);
		
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID_index);
		glDrawElements(GL_TRIANGLES, numOfIndices, GL_UNSIGNED_SHORT, 0);
		
		glDisableVertexAttribArray(VERTEX_INDEX);
		if (this.colors != null) glDisableVertexAttribArray(COLOR_INDEX);
		if (this.normals != null) glDisableVertexAttribArray(NORMAL_INDEX);
		if (this.texCoords != null) glDisableVertexAttribArray(TEX_INDEX);
		
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
		glBindVertexArray(0);
		
		glUseProgram(0);
		
	}
	
	public void delete()
	{
		
		glDeleteBuffers(vboID);
		glDeleteVertexArrays(vaoID);
		glDeleteTextures(texID);
		
	}
	
	public float getRadius(float[] vertices)
	{
		float radius = 0;
		float tempRadius;
		
		for (int i = 0; i < vertices.length; i += 3)
		{
			
			tempRadius = (float) Math.sqrt(Math.pow(vertices[i], 2) + Math.pow(vertices[i + 1], 2) + 
					Math.pow(vertices[i + 2], 2));
			
			if (tempRadius > radius) radius = tempRadius;
			
		}
		
		return radius;
		
	}

	public Vector3f getPos() {
		return pos;
	}

	public void setPos(Vector3f pos) {
		this.pos = pos;
	}

	public Vector3f getAng() {
		return ang;
	}

	public void setAng(Vector3f ang) {
		this.ang = ang;
	}

	public Vector3f getScale() {
		return scale;
	}

	public void setScale(Vector3f scale) {
		this.scale = scale;
	}
	
	public void createBuffers(short[] indices, float[] vertices, float[] colors, float[] texCoords, float[] normals)
	{
		setIndices(indices);
		setVertices(vertices);
		this.radius = getRadius(vertices);
		if (colors != null) 
		{
			setColors(colors);
		}else
		{
			this.colors = null;
		}
		if (texCoords != null) setTexCoords(texCoords);
		if (normals != null) setNormals(normals);
	}
	
	public void setIndices(short[] indices)
	{
		this.indices = BufferUtils.createShortBuffer(indices.length);
		this.indices.put(indices);
		this.indices.flip();
		this.numOfIndices = indices.length;
	}
	
	public void setVertices(float[] vertices)
	{
		this.vertices = BufferUtils.createFloatBuffer(vertices.length);
		this.vertices.put(vertices);
		this.vertices.flip();
		this.numOfVertices = vertices.length;
	}
	
	public void setColors(float[] colors)
	{
		this.colors = BufferUtils.createFloatBuffer(colors.length);
		this.colors.put(colors);
		this.colors.flip();
	}
	
	public void setTexCoords(float[] texCoords)
	{
		this.texCoords = BufferUtils.createFloatBuffer(texCoords.length);
		this.texCoords.put(texCoords);
		this.texCoords.flip();
	}
	
	public void setNormals(float[] normals)
	{
		this.normals = BufferUtils.createFloatBuffer(normals.length);
		this.normals.put(normals);
		this.normals.flip();
	}
	
	public void loadPNGTex(File texture)
	{
		
		try
		{
			//System.out.println("Loading tex");
			InputStream in = new FileInputStream(texture);
			PNGDecoder decoder = new PNGDecoder(in);
			
			texWidth = decoder.getWidth();
			texHeight = decoder.getHeight();
			
			
			texture2d = ByteBuffer.allocateDirect(
					4 * decoder.getWidth() * decoder.getHeight());
			decoder.decode(texture2d, decoder.getWidth() * 4, Format.RGBA);
			texture2d.flip();
			
			in.close();
			
		}catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}

}
