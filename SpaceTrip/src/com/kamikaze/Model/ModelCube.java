package com.kamikaze.Model;

public class ModelCube extends Model
{
	
	public static short[] elements = {
		// front
	    0, 1, 2,
	    2, 3, 0,
	    // top
	    3, 2, 6,
	    6, 7, 3,
	    // back
	    7, 6, 5,
	    5, 4, 7,
	    // bottom
	    4, 5, 1,
	    1, 0, 4,
	    // left
	    4, 0, 3,
	    3, 7, 4,
	    // right
	    1, 5, 6,
	    6, 2, 1,
	};
	public static float[] vertices = new float[] {
			// front
		    -1.0f, -1.0f,  1.0f,
		     1.0f, -1.0f,  1.0f,
		     1.0f,  1.0f,  1.0f,
		    -1.0f,  1.0f,  1.0f,
		    // back
		    -1.0f, -1.0f, -1.0f,
		     1.0f, -1.0f, -1.0f,
		     1.0f,  1.0f, -1.0f,
		    -1.0f,  1.0f, -1.0f,
			
	};
	public static float[] colors = new float[]{
			1.0f, 0.0f, 0.0f,
		    0.0f, 1.0f, 0.0f,
		    0.0f, 0.0f, 1.0f,
		    1.0f, 1.0f, 1.0f,
		    
		    // back colors
		    1.0f, 0.0f, 0.0f,
		    0.0f, 1.0f, 0.0f,
		    0.0f, 0.0f, 1.0f,
		    1.0f, 1.0f, 1.0f
	};
	
	public ModelCube()
	{
		
		//super(vertices, colors, elements);
		
		
	}
	
	
	
}
