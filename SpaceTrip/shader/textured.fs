#version 330

in vec2 pass_texCoord;

layout(location = 0) out vec4 outputF;

uniform sampler2D tex_diffuse;

void main() 
{
	
	outputF = texture2D(tex_diffuse, pass_texCoord);
	//outputF = vec4(1,1,1, 1);
	
}