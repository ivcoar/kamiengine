#version 330


in vec3 pos;
in vec3 color;
in vec3 normal;
in vec2 texCoord;

out vec3 pass_color;
out vec2 pass_texCoord;

uniform mat4 mat_projection;
uniform mat4 mat_view;
uniform mat4 mat_model;


void main() 
{
	
	gl_Position = mat_projection * mat_view * mat_model * vec4(pos, 1.0);
	
	pass_color = color;
	pass_texCoord = texCoord;
	
}