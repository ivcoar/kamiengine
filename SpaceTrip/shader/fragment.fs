#version 330

in vec3 pass_color;

layout(location = 0) out vec4 outputF;


void main() 
{

	outputF = vec4(pass_color, 1);
	
}